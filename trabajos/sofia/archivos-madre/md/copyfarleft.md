La pandemia del copyleft

Al parecer necesitábamos una pandemia global para que finalmente
los editores otorgaran acceso abierto a obras. Supongo que deberíamos
decir… ¿gracias?

En mi opinión fue una buena maniobra de relaciones públicas,
¿a quién no le agradan las compañías cuando hacen el bien? Esta
pandemia ha evidenciado su capacidad para fortalecer instituciones
públicas o privadas, sin importar qué tan pobre han realizado
su trabajo o cómo estas nuevas políticas están normalizando la
vigilancia. Pero qué importa, con trabajos puedo vivir de la
edición de libros y nunca he estado involucrado en trabajo gubernamental.

Un interesante efecto secundario de esta «amable» y temporal
apertura es en torno a la autoría. Uno de los argumentos más
relevantes a favor de la propiedad intelectual (PI) es la defensa
de los derechos de los autores a vivir de su trabajo. Las justificaciones
utilitaristas o laboristas de la PI son muy claras en este sentido.
Para la primera, las leyes de PI confieren un incentivo para
la producción cultural y, por tanto, para la así llamada generación
de riqueza. Para la última, los autores y «[e]l trabajo de su
cuerpo y la labor producida por sus manos podemos decir que son
suyos».

Pero para las justificaciones personalistas también el autor
es el sujeto primordial para las leyes de PI. De hecho, esta
justificación no existiría si la autoría no tuviera una relación
íntima y cualitativamente distinta con su trabajo. Sin algunas
concepciones metafísicas o teológicas sobre la producción cultural,
esta relación especial sería difícil de probar —pero esa es otra
historia—.

IMAGEN: Locke y Hegel bebiendo el té mientras discuten diversos
temas en Nadalandia…

Desde los movimientos del copyfight, copyleft y copyfarleft,
mucha gente ha discutido que este argumento oculta el hecho de
que la mayoría de los autores no pueden vivir de su trabajo,
mientras que los editores y los distribuidores ganan bastante.
Algunos críticos demandan que los gobiernos deberían dar más
poder a los «creadores» en lugar de permitir que los «reproductores»
hagan lo que quieran. No soy fan de esa manera de hacer las cosas
porque no pienso que nadie debiera tener más poder —incluyendo
a los autores— sino distribuirlo, así como en mi mundo el gobierno
es sinónimo de corrupción y muerte. Pero la diversidad de opiniones
es importante, solo espero que no todos los gobiernos sean así.

Así que entre los defensores del copyright, copyfight, copyleft
y copyfarleft de manera usual hay un misterioso consentimiento
acerca de la relevancia del productor. El desacuerdo subyace
en cómo este panorama sobre la producción cultural se traduce
o debería verterse en políticas, legislaciones u organización
política.

En tiempos de emergencia y de crisis estamos viendo qué tan fácil
es hacer una «pausa» sobre estas discusiones y leyes —o acelerar
otras—. Del lado de los gobiernos de nuevo se muestra cómo el
copyright y los derechos de autor no son leyes naturales ni se
apoyan más allá de los sistemas políticos y económicos. Del lado
de los defensores de estos derechos, el fenómeno pone en claro
que la autoría es un argumento que no depende de los productores
de carne y hueso, el fenómeno cultural y cuestiones globales…
Y también evidencia que hay bibliotecarios e investigadores luchando
a favor de intereses públicos; es decir, cuán importantes hoy
en día son las bibliotecas y el acceso abierto y cómo no pueden
ser resplazados por las librerías (en línea) o la investigación
por suscripción.

Me parece muy pretencioso que ciertos autores y editores no estuvieran
de acuerdo con esta apertura temporal de su trabajo. Pero no
perdamos el punto: esta pandemia global ha demostrado qué tan
sencillo es para los editores y los distribuidores optar por
la apertura o los muros de pago —¿a quién le importa los autores?—…
Así que la próxima ves que defiendas el copyright o los derechos
de los autores a vivir de su trabajo, piénsalo dos veces, solo
unos pocos han sido capaces de tener un sustento de vida y, mientras
piensas que los estás ayudando, en realidad estás haciendo más
ricos a terceros.

Al final los titulares de los derechos reservados no son los
únicos que defienden sus intereses al hablar de la importancia
de la gente —en su caso los autores, pero de manera más general
y secular sobre los productores—. Los titulares de obras con
copyleft —una versión «chida» de titulares de derechos que hackearon
las leyes de copyright— también defienden sus intereses de manera
similar pero, en lugar de autores, hablan sobre los usuarios
y, en lugar de ganancias, ellos supuestamente defienden la libertad.

Existe una enorme diferencia entre cada posición, pero solo quiero
denotar cómo hablan de la gente para defender sus intereses.
No los pondría en el mismo saco si no fuera por dos cuestiones.

Algunos titulares de copyleft fueron muy fastidiosos al defender
a Stallman. Weyes, al menos desde aquí no reducimos el movimiento
del software libre a una persona, sin importar si es el fundador
o cuán inteligente o importante ha sido o alguna vez fue. La
crítica a sus acciones no es sinónimo de tirar a la basura lo
que este movimiento ha hecho —¡lo que hemos logrado!—, como muchos
de ustedes trataron de mitigar el asunto al indicar: «Oh, pero
él no es el movimiento, no deberíamos hacer un gran problema
sobre ello». Su actitud y la tuya son el pinche problema. Ambas
dejan en claro lo estrecho de sus miras. Stallman la cagó y se
estaba comportando de una manera muy inmadura al pensar que el
movimiento es gracias a él o que alguna vez lo fue —nosotros
también tenemos nuestras propias historias sobre su comportamiento—,
¿por qué simplemente no lo aceptamos?

Pero en realidad no me importa. Para mí y las personas con las
que trabajo, el movimiento del software libre es un comodín en
donde se reúnen los esfuerzos relativos a la tecnología, política
y cultura para mejores mundos. Sin embargo, la FSF, la OSI, CC
y otras instituciones grandes dentro del copyleft no parecen
darse cuenta que una pluralidad de mundos implica una diversidad
de concepciones acerca de la libertad. O peor aún, han cometido
el común error cuando hablamos acerca de la libertad: olvidan
que «la libertad quiere ser libre».

En su lugar, han tratado de dar definiciones formales a la libertad
del software. No lo tomes a mal, las definiciones son un buen
camino para planear y entender un fenómeno. Pero además de su
formalización, es problemático atar a otros a tus propias definiciones,
principalmente cuando dices que el movimiento es acerca de ellos
y para sus necesidades.

Entre todos los conceptos, la libertad es muy truculenta de definir.
¿Cómo puedes delimitar una idea en una definición cuando el concepto
en sí llama a la incapacidad, quizá, de cualquier atadura? No
es que la libertad no pueda ser definida —de hecho estoy asumiendo
una definición de esta—, sino cuán general y estática puede ser.
Si el mundo muta, si las personas cambian, si el mundo es en
realidad un conjunto de mundos y si la gente se comporta de una
manera y a veces de otra, por supuesto que la noción de libertad
va a variar.

Podríamos intentar reducir la diversidad de los diferentes significados
de la libertad para que pueda ser empotrado en cualquier contexto
o podríamos intentar otra cosa. No lo sé, tal vez podríamos hacer
de la libertad del software un concepto interoperativo que se
adecúe a cada uno de nuestros mundos o simplemente podríamos
dejar de intentar tener un mismo principio.

Las instituciones del copyleft que mencioné y demás compañías
que se enorgullecen de apoyar al movimiento tienden a no ver
esto. Hablo desde mis experiencias, mis luchas y mis angustias
cuando decidí usar licencias copyfarleft en la mayoría de mi
trabajo. En lugar de recibir apoyo de los representantes de estas
instituciones, primero recibí advertencias: «La libertad de la
que hablas no es libertad». Después, cuando busqué su apoyo para
infraestructura, obtuve rechazos: «Estás invitado a usar nuestro
código en tu servidor, pero no podemos hospedarte porque tus
licencias no son libres». Compas, no hubiera buscado su ayuda
en primer lugar si pudiera, dah.

Gracias a muchos hackers y piratas latinoamericanos, poco a poco
estoy construyendo mi infraestructura junto con las suyas. Pero
sé que esta ayuda es más bien un privilegio: por muchos años
no pude ejecutar proyectos o ideas solo porque no tenía acceso
a la tecnología o a tutores. Y peor aún, no tenía capacidad de
observar desde un horizonte más amplio y complejo sin todo este
aprendizaje.

(Existe una deficiencia pedagógica en el movimiento del software
libre que induce a las personas a pensar que es suficiente con
escribir documentación y elogiar el aprendizaje autodidacta.
Desde mi punto de vista, es más bien la producción de una autoimagen
sobre cómo debe ser un hacker o pirata. Además, da mucho pinche
miedo cuando te das cuenta que tan masculino, jerárquico y meritocrático
puede llegar a ser este movimiento).

Según los compas del copyleft, mi noción de libertad del software
no es libre porque las licencias copyfarleft impiden a las personas
usar el software. Esta es una crítica común para cualquier licencia
copyfarleft. Y también es una muy paradójica.

Entre el movimiento del software libre y la iniciativa del código
abierto ha existido un desacuerdo acerca de si se debería heredar
el mismo tipo de licencia, como la Licencia Pública General.
Para el movimiento del software libre esta cláusula asegura que
el software siempre será libre. Según la iniciativa del código
abierto esta cláusula en realidad es una contralibertad porque
no permite a las personas decidir el tipo de licencia a usar
y debido a que es poco atractiva para el emprendimiento empresarial.
No olvidemos que las instituciones de ambos lados asienten con
el carácter esencial del mercado para el desarrollo tecnológico.

Las personas que apoyan el movimiento del software libre tienden
a desvanecer la discusión al declarar que los defensores del
código abierto no entienden las implicaciones sociales de la
cláusula hereditaria o que tienen diferentes intereses y maneras
de cambiar el desarrollo tecnológico. Así que es un tanto paradójico
que estos compas vean la cláusula anticapitalista de las licencias
copyfarleft como una contralibertad. O no entienden sus implicaciones
o no perciben que el copyfarleft no habla del desarrollo tecnológico
en su insolación, sino en sus relaciones políticas, sociales
y económicas.

No voy a defender al copyfarleft de este criticismo. Primero,
no pienso que he de hacer una defensa porque no estoy diciendo
que deberían asir esta noción de libertad. Segundo, tengo una
dura opinión en contra del usual reduccionismo jurídico sobre
este debate. Tercero, pienso que deberíamos enfocarnos en las
maneras en como podemos trabajar en conjunto, en lugar de poner
atención a lo que nos divide. Por último, no pienso que esta
crítica sea incorrecta sino incompleta: la definición de la libertad
del software ha heredado el problema filosófico de cómo definir
la libertad y lo que esta definición implica.

Esto no quiere decir que me desinteresa esta discusión. Se trata
de un tema que me es familiar. El copyright me ha bloqueado el
acceso a la tecnología y el conocimiento con sus muros de pago,
mientras que el copyleft con los mismos efectos me ha puesto
un embargo con sus «muros de licencias». Así que tomemos un momento
para ver qué tan libre es la libertad que predican las instituciones
del copyleft.

Según Open Source Software & The Department of Defense (Programas
de código abierto y el Departamento de Defensa; DoD por sus siglas
en inglés), el DoD estadunidense es uno de los más grandes consumidores
de código abierto. Para ponerlo en perspectiva, todos los vehículos
tácticos del ejército estadunidense usa al menos un pedazo de
software de código abierto en su programación. Otros ejemplos
pueden ser el uso de Android para dirigir ataques aéreos o el
uso de Linux en las estaciones terrestres que operan los drones
militares como el Predator o el Reaper —«predator» de «predador»
y «reaper» también quiere decir «parca» en inglés—.

IMAGEN: Drones Reaper bombardeando de manera incorrecta a civiles
en Afganistán, Irak, Pakistán, Siria y Yemen para repartir la
noción de libertad del Departamento de Defensa de Estados Unidos.

Antes de que argumentes que este es un problema del software
de código abierto y no del software libre, deberías revisar la
sección de preguntas frecuentes del DoD estadunidense. Ahí ellos
definen al software de código abierto como «un programa cuyo
código fuente legible por humanos está disponible para su uso,
estudio, reuso, modificación, mejoramiento y redistribución por
los usuarios de ese programa». ¿Acaso te suena familiar? ¡Por
supuesto!, ellos incluyen la GPL como una licencia de software
abierto y hasta establecen que «un programa de código abierto
también debe satisfacer la definición de software libre del proyecto
GNU».

Este reporte fue publicado en 2016 por el Centro para una Nueva
Seguridad Americana (CNAS, por sus siglas en inglés), un instituto
de investigación de derecha cuya misión y agenda está «diseñada
para dar forma a las decisiones de los líderes del gobierno estadunidense,
el sector privado y la sociedad en pos de los intereses y estrategia
de Estados Unidos».

Encontré este reporte después de leer sobre cómo el ejército
estadunidense dio un pasó atrás al acuerdo por un millardo de
dólares para la adquisición de la «Cúpula de Hierro» después
de que Israel se rehusó a compartir su código (fuente en inglés).
Me pareció interesante que incluso el autodenominado ejército
más poderoso del mundo quedó deshabilitado por cuestiones de
leyes de copyright —un potencial recurso para guerras asimétricas—.
Para mi sorpresa, esta no es una anormalidad.

La intención del reporte hecho por el CNAS es convencer al DoD
estadunidense ha adoptar más software de código abierto porque
«de manera general es mejor que su contraparte propietaria […]
debido a que se puede tomar ventaja del poder mental de grandes
equipos, lo que conlleva a una innovación más rápida, a una mejor
calidad y a una superior seguridad por una fracción del costo».
Este reporte tiene sus orígenes en la «justificada» preocupación
«acerca de la erosión de la superioridad técnica del ejército
de Estados Unidos».

¿Quién habría de pensar que esto le podría ocurrir al software
libre o de código abierto (FOSS, por sus siglas en inglés)? Bueno,
a todos nosotros que desde esta parte del mundo hemos estado
diciendo que el tipo de libertad avalada por muchas instituciones
del copyleft es muy amplia, contraproducente para sus propios
objetivos y, por supuesto, inaplicable para nuestro contexto,
porque esa noción liberal de la libertad del software depende
de instituciones con legitimidad y de la capacidad de que cada
quien tenga propiedad o pueda capitalizar su conocimiento. Ellos
son los mismos que han tratado de explicarnos los modelos económicos
que tratan de «enseñarnos» pero que no funcionan aquí o de los
que dudamos debido a sus efectos secundarios. El micromecenazgo
no es fácil de llevar a cabo aquí porque nuestra producción cultural
depende demasiado de apoyos gubernamentales y sus políticas,
en lugar de vincularse con los sectores privado o público. Y
las donaciones no son buena idea por los intereses ocultos que
pueden tener, así como la dependencia económica que generan.

Pero supongo que su burbuja tiene que rasgarse para que entiendan
el punto. Por ejemplo, las donaciones controversiales realizadas
por Epstein al MIT Media Lab o su amistad con algunas personas
de CC; o el uso del software de código abierto por el Servicio
de Inmigración y Control de Aduanas de los Estados Unidos. Mientras
que por décadas el FOSS ha sido un mecanismo que facilita el
asesinato de los ciudadanos del «sur global»; una herramienta
para la explotación laboral china denunciada por el movimiento
anti-996; un muro de licencias para el acceso a la tecnología
y el conocimiento de personas que no pueden costearse infraestructura
y el aprendizaje que desata, sin importar que el código es «libre»
de usar, o la policía de la libertad del software que niega a
América Latina y otras regiones su derecho a autodeterminar su
libertad, sus políticas sobre el software y sus modelos económicos.

Esas instituciones del copyleft que tanto les importan las «libertades
de los usuarios» en realidad no han sido explícitas acerca de
cómo el FOSS está ayudando a dar forma a un mundo donde muchos
de nosotros no tenemos cabida. Tuvieron que ser centros de investigación
de derecha los que declararon la relevancia del FOSS para el
arte de la guerra, la inteligencia, la seguridad y los regímenes
autoritarios, mientras que estas instituciones se han esforzado
en justificar su comprensión de la producción cultural como la
mercantilización de su capacidad política. En su búsqueda para
que gobiernos y corporaciones adopten el FOSS han hecho evidente
que, cuando favorece a sus intereses, hablan de «libertad de
los usuarios de software» aunque más bien se refieran a la «libertad
de uso del software», sin importar quién es su usuario ni para
qué ha sido usado.

Existe una disonancia cognitiva entre quienes apoyan el copyleft
que los hace ser muy duros con otros —los que solo quieren alguna
ayuda— bajo el argumento sobre si una licencia o un producto
es libre o no. Mientras tanto, no desafían la adopción del FOSS
hecha por cualquier corporación, e incluso las acogen, sin importar
que explote a sus empleados, vigile a sus usuarios, ayude a dinamitar
instituciones democráticas o forme parte de una máquina para
matar.

En mi opinión el término «uso» es uno de los conceptos centrales
que diluye la capacidad política del FOSS hacia una estetización
de su actividad. La espina de las libertades del software recaen
en sus cuatro libertades: las de ejecución, estudio, redistribución
y mejora del programa de cómputo. Aunque Stallman, sus pupilos,
la FSF, la OSI, CC y más siempre indiquen la relevancia de las
«libertades del usuario», estas cuatro libertades no están directamente
relacionadas a sus usuarios. En su lugar, estas son cuatro distintos
casos de uso.

La diferencia no es minúscula. Un caso de uso neutraliza y reifica
al sujeto de su acción. Cuando se diluyen sus intereses, el sujeto
se vuelve irrelevante. Las cuatro libertades no prohíben que
un programa se use de manera egoísta, asesina o autoritaria.
Aunque tampoco fomentan esos usos. Mediante la idea romantizada
de un bien común es fácil pensar que las libertades de ejecución,
estudio, redistribución o mejora de un programa son sinónimos
de un mecanismo que aumenta el bienestar y la democracia. Pero
debido a que estas cuatro libertades no se relacionan a ningún
interés del usuario y en su lugar hablan sobre los intereses
de uso de software y la adopción de una producción cultural «abierta»,
esta oculta el hecho de que la libertad de uso en ciertas ocasiones
va en contra de los sujetos, incluso hasta utilizarlos.

Entonces, el argumento que señala cómo el copyfarleft niega a
las personas el uso de software solo tiene sentido entre dos
equívocos. Primero, la personificación de las instituciones —como
aquelas que alimentan regímenes autoritarios, perpetúan la explotación
laboral o vigilan a sus usuarios— y sus términos de uso que en
ocasiones constriñen la libertad de las personas o el acceso
a su tecnología. Segundo, el supuesto de que las libertades sobre
los casos de uso es igual a la libertad de sus usuarios.

Más bien, si tu modelo económico «abierto» requiere de las libertades
de los casos de uso del software en lugar de las libertades de
sus usuarios, estamos ya muy lejos de la típica discusión sobre
la producción cultural. Me parece muy difícil defender mi apuesta
por la libertad si mi trabajo permite ciertos usos que podrían
ir en contra de la libertad de otros. Por supuesto este es un
dilema de la libertad relativa a la paradoja de la tolerancia.
Pero mi principal conflicto es cuando las personas que apoyan
el copyleft se jactan sobre su defensa a la libertad de los usuarios
mientras hacen micromanagement en torno a las definiciones de
la libertad del software de otros y, al mismo tiempo, dan su
espalda a las zonas grises, oscuras o rojas sobre las implicaciones
de la libertad que tanto procuran. O no les importamos o sus
privilegios les impiden tener empatía.

Desde El manifiesto de GNU queda clara la relevancia que tiene
la industria entre los desarrolladores de software. No cuento
con una respuesta que podría tranquilizarlos. Cada vez se está
volviendo más claro que la tecnología no es un mero instrumento
que pueda ser usado o abusado. La tecnología, o al menos su desarrollo,
es un tipo de praxis política. La incapacidad de la legislación
para hacer valer las leyes mientras que las posibilidades de
las nuevas tecnologías permiten mantener al statu quo, así como
recurren a su auxilio, expresan la capacidad política que tienen
estas tecnologías de la comunicación y la información.

Así como el copyleft hackeó la ley de copyright, con el copyfarleft
podríamos ayudar a desarticular las estructuras del poder o podríamos
inducir a la desobediencia civil. Al prohibir que nuestro trabajo
sea usado con fines militares, policiacos u oligárquicos, podríamos
forzarlos a que dejen de tomar ventaja y a aumentar sus costos
de mantenimiento. Estas instituciones podrían incluso alcanzar
el punto donde no puedan operar más o les sea imposible ser tan
efectivas como nuestras comunidades.

Sé que suena como a una utopía porque en la práctica necesitamos
el esfuerzo de muchas personas involucradas en el desarrollo
tecnológico. Pero ya lo hicimos una vez: usamos la ley de copyright
en contra de sí misma e introducimos un nuevo modelo de distribución
de la fuerza de trabajo y los medios de producción. De nueva
cuenta podríamos usar el copyright para nuestro beneficio, pero
ahora en contra de las estructuras de poder que vigilan, explotan
o matan personas. Estas instituciones necesitan nuestro «poder
mental», podemos intentar con rehusárselo. Algunas exploraciones
podrían ser licencias de software que de manera explícita prohíban
la vigilancia, la explotación o el asesinato.

También podríamos dificultarles el robo de nuestro desarrollo
tecnológico y negarles el acceso a nuestras redes de comunicación.
Hoy en día los modelos de distribución del FOSS han confundido
la economía abierta con la economía del regalo. Otro instituto
—el Centro de Investigación de Economía y Política Exterior (EDAM,
por sus siglas en inglés)— publicó un reporte —Digital Open Source
Intelligence Security: A Primer (Inteligencia de seguridad digital
con fuentes abiertas: una introducción)— donde indica que las
fuentes abiertas constituyen «al menos un 90%» de todas las actividades
de inteligencia. Esto incluye nuestra producción publicada de
manera abierta y los estándares abiertos que desarrollamos en
pos de la transparencia. Por este motivo la encriptación punto
a punto es importante y por eso deberíamos extender su uso en
lugar de permitir que los gobiernos la prohíban.

El copyleft podría ser una pandemia global si no hacemos algo
en contra de su incorporación dentro de las virulentas tecnologías
de la destrucción. Necesitamos una mayor organización para que
el software que desarrollamos sea «libre como en libertad social
y no solo como individuo libre».
